# Douby What?

During Inktober, someone had the idea to make it linocut. Then the same person asked me to make a game about it, when the goal to have a scoreboard to make the winner choose and receive a douby linocut print they want.

It has been developped during a rush coding session where dirty but working code is allowed, don't be mad :)

Douby Runner Open Source version is shared without the official Douby assets.

# Dev Hints

## Fichiers

 * Jeu: sketch.js
 * Score: score.php + scoreboard.php (+ credential.php)
 * Le reste: index.html + ihm.js
## TODOs

 * [ ] Optimize for bad GPU users
 * [ ] Optimize for mobile devices
 * [ ] Make harder for people to submit cheat score (ATM it's just a form post)
 
# Ressources
Utilise [p5.js](https://github.com/processing/p5.js) et [animate.css](https://animate.style/)

# Licence
MIT License
