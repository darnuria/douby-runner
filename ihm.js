function unModal(modale) {
    var modal = document.getElementById(modale);
    modal.style.display = "none";

}

function modal(modale) {
    var modal = document.getElementById(modale);
    modal.style.display = "block";
}

function suivant(step) {
    var astuce = document.getElementById('welcome-' + (step));
    var astuceNext = document.getElementById('welcome-' + (step + 1));
    astuce.classList.add("hidden");
    astuceNext.classList.remove("hidden");
}

function lancer() {
    if (document.getElementById("contact").value == '') {
        if (!confirm("Tu n'as pas saisis tout les champs.\nTu peux continuer comme cela, mais tu participeras pas au jeu concours.\nContinuer quand même ?")) {
            return false;
        }
        document.getElementById("nick").value = 'DoubyLover';
        document.getElementById("contact").value = '@DoubyLover';
    }
    if (document.getElementById("nick").value == '') {
        document.getElementById("nick").value = 'DoubyShadow';
    }
    unModal('modal-welcome');
    reStart();
}

function die() {
    var ending = Math.floor(Math.random() * 4) + 1;
    var ends = document.getElementsByClassName("ending");
    for (var i = 0; i < ends.length; i++) {
        ends[i].classList.add("hidden");
    }
    var theEnd = document.getElementById('end-' + (ending));
    theEnd.classList.remove("hidden");
    theEnd.classList.remove("hidden");
    modal('modal-dead');

    scoreAndScoreboard();
}

function respaune() {
    unModal('modal-dead');
    reStart();
}

function credit() {
    modal('modal-credit');
}

function scoreboard() {
    var loadscoreboard = document.getElementById('loadScore');
    loadscoreboard.classList.remove("hidden");
    var closescoreboard = document.getElementById('btn-fermer-score');
    closescoreboard.classList.add("hidden");
    var scoreboard = document.getElementById('scoreboard');
    scoreboard.classList.add("hidden");
    modal('modal-score');
    getScoreBoard();
}

function scoreAndScoreboard() {
    var loadscoreboard = document.getElementById('loadScore');
    loadscoreboard.classList.remove("hidden");
    var closescoreboard = document.getElementById('btn-fermer-score');
    closescoreboard.classList.add("hidden");
    var scoreboard = document.getElementById('scoreboard');
    scoreboard.classList.add("hidden");
    modal('modal-score');
    setScore();

}

function fermer(laModale) {
    unModal('modal-' + laModale);
}

function setScore() {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", 'score.php', true);

    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            console.log(xhr.response);
            if (xhr.response == '1') {
                getScoreBoard();
            } else {
                alert("Désolé, une erreur est survenue lors de la sauvegarde du score");
                document.location.reload();
            }

        }
    }
    xhr.send('nick=' + document.getElementById("nick").value +
        '&contact=' + document.getElementById("contact").value +
        '&score=' + document.getElementById("lescore").value +
        '&when=' + document.getElementById("when").value +
        '&token=' + document.getElementById("token").value
    );

}

function getScoreBoard() {
    var url = 'scoreboard.php?contact=' + document.getElementById("contact").value;
    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            var msg = '<div id="scoreboard">' + xhr.response + '</div>'
            var scoreboard = document.getElementById('scoreboard');
            if (scoreboard.outerHTML) { //if outerHTML is supported
                scoreboard.outerHTML = msg;
            } else { //if outerHTML is not supported, there is a weird but crossbrowsered trick
                var tmpObj = document.createElement("div");
                tmpObj.innerHTML = '<!--THIS DATA SHOULD BE REPLACED-->';
                ObjParent = scoreboard.parentNode; //Okey, element should be parented
                ObjParent.replaceChild(tmpObj, scoreboard); //here we placing our temporary data instead of our target, so we can find it then and replace it into whatever we want to replace to
                ObjParent.innerHTML = ObjParent.innerHTML.replace('<div><!--THIS DATA SHOULD BE REPLACED--></div>', msg);
            }
            var loadscoreboard = document.getElementById('loadScore');
            loadscoreboard.classList.add("hidden");
            scoreboard.classList.remove("hidden");
            var closescoreboard = document.getElementById('btn-fermer-score');
            closescoreboard.classList.remove("hidden");
        }
    }

    xhr.open('GET', url, true);
    xhr.send('');
}