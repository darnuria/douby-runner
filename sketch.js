let score = 0;
let scroll = 10;
let scrollBg = 0;
let trains = [];
let trainsMedia = [];
let bg = [];
let douby;
let failSounds = [];
let jumper = [];
let restart = false;
let baseUrl = './media';
let pauseGame = true;

function preload() {
    music = loadSound(getMediaName('ground', 'ambiance', 'all', 'all', 'all', 'sound'));
    jump = [];
    jump[0] = loadSound(getMediaName('douby', 'jump', 'sol', 'A', 1, 'sound'));
    jump[1] = loadSound(getMediaName('douby', 'jump', 'underground', 'A', 1, 'sound'));
    jump[100] = loadSound(getMediaName('douby', 'score', '100', 'A', 1, 'sound'));
    jump[500] = loadSound(getMediaName('douby', 'score', '500', 'A', 1, 'sound'));

    colision = [];
    colision['default'] = loadSound(getMediaName('obstacles', 'colision', 'all', 'all', 'all', 'sound'));
    colision['bonus'] = loadSound(getMediaName('obstacles', 'colision', 'all', 'B', 'all', 'sound'));
    colision['to_sol'] = loadSound(getMediaName('obstacles', 'colision', 'sol', 'P', 1, 'sound'));
    colision['to_underground'] = loadSound(getMediaName('obstacles', 'colision', 'underground', 'P', 1, 'sound'));

    bg[getMediaId('ground', 'bg', 'sol', 'A', 1, 'img')] = loadImage(getMediaName('ground', 'bg', 'sol', 'A', 1, 'img'));
    bg[getMediaId('ground', 'bg', 'underground', 'A', 1, 'img')] = loadImage(getMediaName('ground', 'bg', 'underground', 'A', 1, 'img'));
    scoreBg = loadImage(getMediaName('ground', 'score', 'all', 'all', 'all', 'img'));

    for (let i = 1; i <= 3; i++) {
        jumper[getMediaId('douby', 'jump', 'sol', 'A', i, 'img')] = loadImage(getMediaName('douby', 'jump', 'sol', 'A', i, 'img'));
        jumper[getMediaId('douby', 'jump', 'underground', 'A', i, 'img')] = loadImage(getMediaName('douby', 'jump', 'underground', 'A', i, 'img'));
        jumper[getMediaId('douby', 'run', 'sol', 'A', i, 'img')] = loadImage(getMediaName('douby', 'run', 'sol', 'A', i, 'img'));
        jumper[getMediaId('douby', 'run', 'underground', 'A', i, 'img')] = loadImage(getMediaName('douby', 'run', 'underground', 'A', i, 'img'));
    }
    i = 1;
    trainsMedia[getMediaId('obstacles', 'arrive', 'sol', 'A', i, 'img')] = loadImage(getMediaName('obstacles', 'arrive', 'sol', 'A', i, 'img'));
    trainsMedia[getMediaId('obstacles', 'arrive', 'sol', 'B', i, 'img')] = loadImage(getMediaName('obstacles', 'arrive', 'sol', 'B', i, 'img'));
    trainsMedia[getMediaId('obstacles', 'arrive', 'sol', 'C', i, 'img')] = loadImage(getMediaName('obstacles', 'arrive', 'sol', 'C', i, 'img'));
    trainsMedia[getMediaId('obstacles', 'arrive', 'sol', 'D', i, 'img')] = loadImage(getMediaName('obstacles', 'arrive', 'sol', 'D', i, 'img'));
    trainsMedia[getMediaId('obstacles', 'arrive', 'sol', 'E', i, 'img')] = loadImage(getMediaName('obstacles', 'arrive', 'sol', 'E', i, 'img'));
    trainsMedia[getMediaId('obstacles', 'arrive', 'sol', 'P', i, 'img')] = loadImage(getMediaName('obstacles', 'arrive', 'sol', 'P', i, 'img'));
    trainsMedia[getMediaId('obstacles', 'arrive', 'underground', 'A', i, 'img')] = loadImage(getMediaName('obstacles', 'arrive', 'underground', 'A', i, 'img'));
    trainsMedia[getMediaId('obstacles', 'arrive', 'underground', 'B', i, 'img')] = loadImage(getMediaName('obstacles', 'arrive', 'underground', 'B', i, 'img'));
    trainsMedia[getMediaId('obstacles', 'arrive', 'underground', 'C', i, 'img')] = loadImage(getMediaName('obstacles', 'arrive', 'underground', 'C', i, 'img'));
    trainsMedia[getMediaId('obstacles', 'arrive', 'underground', 'D', i, 'img')] = loadImage(getMediaName('obstacles', 'arrive', 'underground', 'D', i, 'img'));
    trainsMedia[getMediaId('obstacles', 'arrive', 'underground', 'P', i, 'img')] = loadImage(getMediaName('obstacles', 'arrive', 'underground', 'P', i, 'img'));

}

function setup() {
    createCanvas(windowWidth * 0.98, windowHeight * 0.98);
    douby = new Douby();
    modal('modal-welcome');
}

function keyPressed() {
    if (key == ' ') {
        douby.jump();
        return false;
    }
}

function mouseClicked() {
    douby.jump();
    return false;
}

function touched() {
    douby.jump();
    return false;
}


function reStart() {
    pauseGame = false;
    restart = true;
    jumpstart();
}

function jumpstart() {
    restart = false;
    score = 0;
    scollBg = 0;
    scroll = 10;
    trains = [];
    music.play();
    music.setLoop(true);
    music.setVolume(0.2);
    loop();
    douby.setGround('sol');
    douby.jump();
    return false;
}

function draw() {

    image(bg[getMediaId('ground', 'bg', douby.getGround(), 'A', 1, 'img')], -scrollBg, 0, width, height);
    image(bg[getMediaId('ground', 'bg', douby.getGround(), 'A', 1, 'img')], -scrollBg + width, 0, width, height);
    if (pauseGame) {
        return true;
    }
    if (scrollBg > width) {
        scrollBg = 0;
    }

    if (random(1) < 0.75 && frameCount % 50 == 0) {
        trains.push(new Train())
        trains[Object.keys(trains)[Object.keys(trains).length - 1]].setGround(douby.getGround());
        trains[Object.keys(trains)[Object.keys(trains).length - 1]].setVariante();
    }

    if (score % 100 == 0 && score != 0 && !(score % 500 == 0)) {
        jump[100].play();
    }
    if (score % 500 == 0 && score != 0) {
        jump[500].play();
    }

    if (frameCount % 5 == 0) {
        score++;
    }

    fill(0);
    image(scoreBg, 0, 0, 240, 96);
    textSize(28);
    textFont('Comic Sans MS');
    text(` ${score}`, 114, 57);

    for (let t of trains) {
        t.move();
        t.show();

        if (douby.collide(t)) {
            if (douby.getGround() == 'sol' && t.getVariante() == 'P') { // do we go bottom
                colision['to_underground'].play();
                douby.setGround('underground');
            } else if (douby.getGround() == 'underground' && t.getVariante() == 'P') { // do we go top
                colision['to_sol'].play();
                douby.setGround('sol');
            } else if (t.getVariante() == 'B') { // do we go top
                colision['bonus'].play();
                score += 42;
            } else { // or we just end
                leFin();
            }
            t.r = 0;
            t.x = 0;
        }
    }
    douby.show()
    douby.move()

    scroll += 0.005;
    scrollBg += scroll / 2;
}

function leFin() {
    noLoop()
    music.stop();
    //let sound = random(failSounds)
    //sound.play();
    colision['default'].play();
    document.getElementById("lescore").value = score;
    document.getElementById("when").value = Date.now();
    document.getElementById("token").value = 0;
    fill(0);
    die();
    restart = true;
    pauseGame = true;
}

function getMediaId(objectName, action, ground, variante, step, type) {
    //console.log(objectName + '_' + type + '_' + action + '_' + ground + '_' + variante + '_' + step);
    return objectName + '_' + type + '_' + action + '_' + ground + '_' + variante + '_' + step;
}

function getMediaName(objectName, action, ground, variante, step, type) {
    let types = {
        "img": "png",
        "sound": "mp3"
    };
    //console.log(baseUrl + '/' + objectName + '/' + type + '/' + action + '_' + ground + '_' + variante + '_' + step + '.' + types[type]);
    return baseUrl + '/' + objectName + '/' + type + '/' + action + '_' + ground + '_' + variante + '_' + step + '.' + types[type];
}

class Douby {
    constructor() {
        this.solCorrection = 10;
        this.r = 100;
        this.x = 105;
        this.y = height - this.r;
        this.prevY = 0;
        this.vy = 0;
        this.gravity = 2;
        this.maxGravity = 2;
        this.previousStep = 0;
        this.step = 1;
        this.maxStep = 3;
        this.up = true;
        this.ground = 'sol';
        this.variante = 'A';
        this.action = 'run';
    }

    getGround() {
        return this.ground;
    }

    setGround(ground) {
        this.ground = ground;
    }

    move() {
        this.y += this.vy;
        this.vy += this.gravity;
        this.y = constrain(this.y, 0, height - this.r - this.solCorrection);

        if (this.y == (height - this.r - this.solCorrection)) { // si au sol
            this.action = 'run';
            this.step = (1 + (score % this.maxStep));
        } else {
            //console.log("pY" + this.prevY + "  Y: " + this.y + "     XY: " + this.vy);
            if (this.step == 1 && this.vy > -16 && this.vy < 12) {
                this.step = 2;
            }
            if (this.step == 2 && this.vy > 2) {
                this.step = 3;
            }
        }
    }

    jump() {
        if (this.action == 'jump') {
            return true;
        }
        if (this.y == height - this.r - this.solCorrection) {
            this.vy = -32;
            jump[(Math.random() > 0.5 ? 0 : 1)].play();
            this.gravity = this.maxGravity;
        }
        this.action = 'jump';
        this.step = 1;
    }

    collide(other) {
        let hitX = this.x + this.r > other.x &&
            this.x < other.x + other.r
        let hitY = this.y + this.r > other.y
        return (hitX && hitY)
    }

    show() {
        fill(255, 127);
        //rect(this.x, this.y, this.r, this.r)
        var ajustement = 1;
        if (this.ground == 'underground' && this.action == 'run') {
            ajustement = 1.6;
        }
        image(jumper[getMediaId('douby', this.action, this.ground, this.variante, this.step, 'img')], this.x - this.r * ajustement, this.y - this.r * ajustement * (ajustement != 1 ? 0.8 * ajustement : 1), ajustement * 2 * this.r, ajustement * 2 * this.r);

    }
}

class Train {
    constructor() {
        this.solCorrection = 5;
        this.r = 85;
        this.x = width;
        this.y = height - this.r - this.solCorrection;
        this.step = 1;
        this.maxStep = 1;
        this.ground = 'sol';
        this.variante = 'A';
        this.action = 'arrive';
    }

    getVariante() {
        return this.variante;
    }

    getVariantes() {
        var variantes = new String;
        switch (this.ground) {
            case 'sol':
                variantes = "ABCDEPACDACDD";
                //variantes = "PA";
                break;
            case 'underground':
                variantes = "ABCDPACDACDBB";
                break;
            default:
                variantes = "A";
                break;
        }
        return variantes;
    }

    setVariante() {
        var characters = "ABC";
        characters = this.getVariantes();
        var length = 1;
        var result = '';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        this.variante = result;
        if (this.ground == 'sol' && (this.variante == 'D' || this.variante == 'E')) {
            this.y = 45;
        }
        if (this.ground == 'underground') {
            this.y = height - this.r - (3 * this.solCorrection);
        }
    }

    setGround(ground) {
        this.ground = ground;
    }

    move() {
        this.x -= scroll;
        if (this.ground == 'sol' && (this.variante == 'D' || this.variante == 'E')) { // badbird !
            this.y = height - (1.4 * this.x / Math.log(this.x)) - (0.6 * this.r);
        }
    }

    show() {
        fill(255, 127);
        //rect(this.x, this.y, this.r, this.r)
        image(trainsMedia[getMediaId('obstacles', this.action, this.ground, this.variante, this.step, 'img')], this.x, this.y - this.r, 2 * this.r, 2 * this.r);
        //image(train, this.x, this.y, this.r, this.r)
    }
}